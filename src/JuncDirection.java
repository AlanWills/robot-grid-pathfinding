public class JuncDirection 
{
	// Enum value to store which direction of the junction this instance represents.
	public Direction direction;
	
	// 2-d coordinate to represent the coords of the next node in our direction.
	public int[] positionOfNextNode;
	
	// Heuristic value which will be used to traverse the grid via the optimal route.
	public double heuristic;
	
	// Probably the constructor we will use for this assignment.
	public JuncDirection(Direction dir, int xCoordOfNextNode, int yCoordOfNextNode)
	{
		// Set up the vector for the position of the next node in the direction dir.
		positionOfNextNode = new int[]{xCoordOfNextNode, yCoordOfNextNode};
		direction = dir;
		
		// Set the initial value of the heuristic to be 0.
		heuristic = 0;
	}
	
	// More general constructor which could be used for grids of arbitrary dimensions.
	public JuncDirection(Direction dir, int[] coordOfNextNode)
	{
		positionOfNextNode = coordOfNextNode;
		direction = dir;
		
		// Set the initial value of the heuristic to be 0.
		heuristic = 0;
	}
	
	public void CalculateHeuristic(int sonarReading, int[] position)
	{
		// Calculate the heuristic.
		if (sonarReading < 30)
		{
			// Our sonar tells us there is an object directly along this junction to the next node so we should never travel along here.
			heuristic = Grid.MAX_VALUE;
		}
		else
		{
			// Need to extend this heuristic further.
			heuristic = Grid.destPoint[0] + Grid.destPoint[1] - position[0] - position[1] + ((255 - sonarReading)/255);
		}
	}
}
