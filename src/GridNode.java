// An enum which will be useful when determining which direction we are facing in/analysing.
enum Direction
{
	North,
	East,
	South,
	West
}

public class GridNode 
{
	// Declare an array to store information on our junction's directions.
	public JuncDirection[] junctionDirections;
	
	// Array to store the position of this node in the grid.
	public int[] position;
	
	// Constructor for a GridNode.
	public GridNode(int xCoord, int yCoord)
	{
		// Set the size of our array of JuncDirections to 4.
		junctionDirections = new JuncDirection[4];
		
		// Set up the position array.
		position = new int[]{xCoord, yCoord};
	}
	
	// Sets up all the junction directions.
	public void Initialize()
	{
		junctionDirections[0] = new JuncDirection(Direction.North, position[0], position[1] + 1);
		junctionDirections[1] = new JuncDirection(Direction.East, position[0] + 1, position[1]);
		junctionDirections[2] = new JuncDirection(Direction.South, position[0], position[1] - 1);
		junctionDirections[3] = new JuncDirection(Direction.West, position[0] - 1, position[1]);
	}
}
