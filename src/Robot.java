public class Robot 
{
	public LightSensor leftLightSensor;
	public LightSensor rightLightSensor;
	public UltraSonicSensor sonar;
	public DifferentialPilot pilot;
	
	public int[] position;
	public boolean isFinishedTravelling;
	
	private Grid grid;
	private GridNode currentGridNode;
	private Direction currentHeading;
	private boolean sweptGrid;
	
	public Robot(Grid grid)
	{
		leftLightSensor = new LightSensor(SensorPort.S1);
		rightLightSensor = new LightSensor(SensorPort.S2);
		
		sonar = new UltraSonicSensor(SensorPort.S3);
		sonar.continuous();
		
		// Need to fill in the arguments for this;
		pilot = new DifferentialPilot(5.4f, 15.6f, Motor.B, Motor.C);
		
		// Create a local copy of our grid
		this.grid = grid;
		
		// Set our bool which keeps track of environment mapping to false - only want to do this once.
		sweptGrid = true;
		
		Restart();
	}
	
	// Allows us to restart the pathfinding so long as the grid is the same.
	// We can now use the heuristics we have already calculated to traverse the grid in a more optimal route.
	private void Restart()
	{
		// Set the grid position to be at co-ordinate (0, 0)
		position = new int[]{0, 0};
		
		// Set our current grid node.
		currentGridNode = grid.gridNodes[position[0]][position[1]];
		
		// Set our initial heading direction
		currentHeading = Direction.North;
	}
	
	public void SweepGrid()
	{
		if (!sweptGrid)
		{
			double rotation = 0;
			while (rotation <= 90)
			{
				// Read the sonar
				int distance = sonar.getDistance();
				
				// If the sonar has encountered an obstacle
				if (distance < 200)
				{
					// Work out where in the grid the obstacle is in terms of our grid nodes.
					int xCoord = (Math.cos(rotation) * distance) / 30;
					int yCoord = (Math.sin(rotation) * distance) / 30;
					
					// Mark the corresponding grid node heurstics
				}
				
				rotation++;
			}
			
			sweptGrid = true;
		}
	}
	
	public void Move(Direction dir)
	{
		// Do the moving following the line and then the junction detection and behaviour.
		int lightReadingL = leftLightSensor.readValue();
		int lightReadingR = rightLightSensor.readValue();
		int lightReadingAverage = (lightReadingL + lightReadingR) / 2;
		
		boolean junctionReached = false;
		
		while (junctionReached == false)
		{		
			// Check we haven't reached a junction.
			if (lightReadingAverage >= 42)
			{
				// Check condition that we ARE on the line.
				if (lightReadingL < 52 || lightReadingR < 52)
				{
					pilot.travel(1);
				}
				// We are off the line so find it again.
				else
				{
					FindTape();
				}
			}
			// We have reached a junction so exit this while loop.
			else
			{
				junctionReached == true;
			}
		}
		
		// Move forward 6 so that our robot is over the junction.
		pilot.travel(6);
		
		switch (dir)
		{
			case North :
				position[1]++;
				break;
				
			case South :
				position[1]--;
				break;
				
			case East :
				position[0]++;
				break;
				
			case West :
				position[0]--;
				break;
		}
		
		// Set our current grid node.
		currentGridNode = grid.gridNodes[position[0]][position[1]];
		
		// We have reached the destination point
		if (position == grid.destPoint)
		{
			// Wait for button press.
			// If we have button press then restart - call Restart() and then AnalyseAllJunctions().
			if (Button.waitForButtonPress())
			{
				Restart();
				AnalyseAllJunctions();
			}
		}
		// We are not at our destination so need to analyse our next step.
		else
		{
			AnalyseAllJunctions();
		}
	}
	
	private void FindTape()
	{
		int angleTurned = 0;
		int lightReadingL, lightReadingR, lightReadingAverage;
		
		while (angleTurned >= -45)
		{
			lightReadingL = leftLightSensor.readValue();
			lightReadingR = rightLightSensor.readValue();
			
			// Check condition we haven't found the tape
			if (lightReadingL >= 52 && lightReadingR >= 52)
			{
				angleTurned--;
				pilot.rotate(-1);
			}
			// We have found the tape so we can get back to moving.
			else
			{
				return;
			}
		}
		
		while (angleTurned <= 45)
		{
			lightReadingL = leftLightSensor.readValue();
			lightReadingR = rightLightSensor.readValue();
			
			// Check condition we haven't found the tape
			if (lightReadingL >= 52 && lightReadingR >= 52)
			{
				angleTurned++;
				pilot.rotate(1);
			}
			// We have found the tape so we can get back to moving.
			else
			{
				return;
			}
		}
	}
	
	public void AnalyseAllJunctions()
	{
		// Analyse all the junctions.
		int currentJunction = (currentHeading.ordinal() - 1) % 4;
		AnalyseJunction(currentJunction);
		
		currentJunction = (currentJunction++) % 4;
		AnalyseJunction(currentJunction);
		
		currentJunction = (currentJunction++) % 4;
		AnalyseJunction(currentJunction);
		
		// Get the new heading we will travel in, rotate to that new heading and then move in that direction.
		currentHeading = CalculateNewHeading();
		Rotate(currentHeading);
		Move(currentHeading);
	}
	
	private void AnalyseJunction(int currentJunction)
	{
		// Only calculate the heuristic if we have not already done so.
		if (currentGridNode.junctionDirections[currentJunction].heuristic == 0)
		{
			// Rotate to face the current direction.
			Rotate(currentGridNode.junctionDirections[currentJunction].direction);
			
			// Get the sonar reading to check for distant and near objects.
			int sonarReading = sonar.getDistance();
			
			// Update the heuristic of the appropriate junction direction we are now facing in.
			currentGridNode.junctionDirections[currentJunction].CalculateHeuristic(sonarReading, position);
		}
	}
	
	private Direction CalculateNewHeading()
	{
		// Set initial default value to be north.
		Direction dir = Direction.North;
		double dirHeuristic = currentGridNode.junctionDirections[0].heuristic;
		
		// Analyse the three other junctions and return the one with the lowest heuristic.
		for (int i = 1; i < 4; i++)
		{
			// Our heuristic equation is designed so that equality is never a possibility.
			// In this case, the direction we are analysing has a lower heuristic so we should move in this direction instead.
			// Otherwise, the direction we are currently going to move in is better than the one we are analysing.
			// Need to remove the = sign when we fix the heuristic function.
			if (currentGridNode.junctionDirections[i].heuristic <= dirHeuristic)
			{
				dir = currentGridNode.junctionDirections[i].direction;
				dirHeuristic = currentGridNode.junctionDirections[i].heuristic;
			}
		}
		
		// Return the lowest cost junction.
		return dir;
	}
	
	// Rotate from our currentHeading to the inputed heading.  Should only really be used to rotate a max of 90 degrees.
	private void Rotate(Direction dir)
	{
		// -1 = rotate clockwise, 1 = rotate anti-clockwise.
		int leftOrRight = (currentHeading.ordinal() - dir.ordinal()) % 4 == 1 ? 1 : -1;
		
		// Rotate the robot until it hits the line in the appropriate direction using the leftOrRight variable to indicate direction.
		// Will need to check that positive rotation is anti-clockwise.
		int angleTurned = 0;
		int lightReadingL, lightReadingR, lightReadingAverage;
		boolean lineFound = false;
		
		while (!lineFound)
		{
			lightReadingL = leftLightSensor.readValue();
			lightReadingR = rightLightSensor.readValue();
			lightReadingAverage = (lightReadingL + lightReadingR) / 2;
			
			// Check condition that robot found the line
			if (lightReadingL < 52 || lightReadingR < 52)
			{
				if (angleTurned > 30)
				{
					lineFound = true;
					return;
				}
			}
			else
			{
				angleTurned++;
				pilot.Rotate(leftOrRight * 1);
			}
		}
		
		currentHeading = dir;
	}
}
