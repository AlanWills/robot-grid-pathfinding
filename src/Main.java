
public class Main 
{
	public static int xDims = 4;
	public static int yDims = 4;
	public static int destX = xDims - 1;
	public static int destY = yDims - 1;
	public static Grid grid;
	
	public static Robot robot;
	
	public static void main(String[] args)
	{
		SetUpGrid();
		SetUpRobotAndSweepGrid();
		robot.AnalyseAllJunctions();
	}
	
	public static void SetUpGrid()
	{
		grid = new Grid(xDims, yDims, destX, destY);
		grid.Initialize();
	}
	
	public static void SetUpRobotAndSweepGrid()
	{
		robot = new Robot(grid);
		// robot.SweepGrid();
	}
}
