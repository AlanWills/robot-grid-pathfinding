
public class Grid 
{
	// Declare a matrix of GridNodes which will be our grid later on.
	public GridNode[][] gridNodes;
	public int[] dimensions;
	public static int[] destPoint;
	
	public static double MAX_VALUE = 2^20; 
	
	// Constructor for the Grid.
	public Grid(int grid_dims_x, int grid_dims_y, int destX, int destY)
	{
		// Store the numeric dimensions of our grid.
		dimensions = new int[]{grid_dims_x, grid_dims_y};
		
		// Create the destination point we are trying to get to.
		destPoint = new int[]{destX, destY};
		
		// Set the dimensions of our grid using the parameters in the constructor.
		gridNodes = new GridNode[grid_dims_x][grid_dims_y];
	}
	
	// Initialize all the GridNodes and set heuristics for all the paths along the edge which have no node at the end.
	public void Initialize()
	{
		for (int y = 0; y < dimensions[1]; y++)
		{
			for (int x = 0; x < dimensions[0]; x++)
			{
				GridNode gridNode = new GridNode(x, y);
				gridNode.Initialize();
				
				gridNodes[x][y] = gridNode;
			}
		}
		
		for (int x = 0; x < dimensions[0]; x++)
		{
			// Set all of the South JuncDirections heuristics along the bottom row of the grid to be MAX_VALUE - to signify infinite weight. 
			gridNodes[x][0].junctionDirections[Direction.South.ordinal()].heuristic = MAX_VALUE;
			
			// Set all of the North JuncDirections heuristics along the top row of the grid to be MAX_VALUE - to signify infinite weight.
			gridNodes[x][dimensions[1] - 1].junctionDirections[Direction.North.ordinal()].heuristic = MAX_VALUE;
		}
		
		for (int y = 0; y < dimensions[1]; y++)
		{
			// Set all of the West JuncDirections heuristics along the left column of the grid to be MAX_VALUE - to signify infinite weight.
			gridNodes[0][y].junctionDirections[Direction.West.ordinal()].heuristic = MAX_VALUE;
			
			// Set all of the East JuncDirections heuristics along the right column of the grid to be MAX_VALUE - to signify infinite weight.
			gridNodes[dimensions[0] - 1][y].junctionDirections[Direction.East.ordinal()].heuristic = MAX_VALUE;
		}
	}
}
